/*

        Transmission frequency collision solver
        Version 1.0

        Module list.c
        This module represents a linked list implemented by storing values
        in array of pointers (Represented by pointer to a pointer). When
        element is inserted and the capacity of the array is exceeded, new
        array is allocated and replaces the smaller array.

        Dialect : ANSI C
        Compiler: any ANSI C-compatible

        Copyright (c) Tomas Linhart, 2019

 */

#include "list.h"
#include "defs.h"
#include <stdlib.h>
#include <stdio.h>

/* ____________________________________________________________________________

    list *create_list()

    This function allocates and initializes space in memory to a new 'list'
    with capacity of 'DEFAULT_LIST_CAPACITY' and returns a pointer to this 'list'
   ____________________________________________________________________________
*/

list *create_list()
{
    list* new_list = calloc(1, sizeof(list));

    if (new_list == NULL) {
        printf("ERR#2: Out of memory!");
        exit(2);
    }

    new_list->capacity = DEFAULT_LIST_CAPACITY;
    new_list->nodes = calloc(new_list->capacity, sizeof(node));
    new_list->count = DEFAULT_LIST_COUNT;
    return new_list;
}

/* ____________________________________________________________________________

    int is_full(list* list)

    This function checks whether given 'lists' count of elements equals to
    capacity of this 'list'
   ____________________________________________________________________________
*/

int is_full(list* list)
{
    return (list->count == list->capacity);
}

/* ____________________________________________________________________________

    void resize_array(list* list)

    This function allocates new space in memory for nodes of capacity
    of the smaller list times 'RESIZE_MULTIPLIER'. This space then replaces
    an old smaller space for nodes used previously and updates value of
    capacity stored in given 'list'
   ____________________________________________________________________________
*/

void resize_array(list* list)
{
    node** biggerArray = realloc(list->nodes, RESIZE_MULTIPLIER * list->capacity * sizeof(node));

    if (biggerArray == NULL) {
        printf("ERR#2: Out of memory!");
        exit(ERRC_OUT_OF_MEMORY);
    }

    list->capacity = RESIZE_MULTIPLIER * list->capacity;
    list->nodes = biggerArray;
}

/* ____________________________________________________________________________

    int add(list *l, void *prvek)

    This function adds an element at the end of given list. First it checks
    whether the array is full and if it is, then it resizes it with function
    'resize_array'. It allocates spaces for new 'node' of the 'list' and adds
    it to the 'list'. If the 'list' is empty, it also puts it to the start
    of the 'list'.
   ____________________________________________________________________________
*/
void add(list *l, void *prvek)
{
    node* n;
    if (is_full(l)) {
        resize_array(l);
    }

    n = malloc(sizeof(node));

    n->data = prvek;
    n->next = NULL; /* In this implementation links to next element aren't used */
    l->nodes[l->count] = n;

    l->count++;
}

/* ____________________________________________________________________________

    void* get(list *list, int index)

    Returns a pointer to the data of a 'node' specified by 'index' in given 'list'
   ____________________________________________________________________________
*/

void* get(list *list, int index)
{
    node* n;

    if (list->count == LIST_COUNT_EMPTY) {         /* If 'list' is empty */
        return NULL;
    }

    n = list->nodes[index];

    return n->data;
}

/* ____________________________________________________________________________

    int count(list *l)

    Returns number of elements stored in a given 'list'
   ____________________________________________________________________________
*/
int count(list *l) {
    return l->count;
}

/* ____________________________________________________________________________

    void free_list(list *l)

    Frees data of list
   ____________________________________________________________________________
*/
void free_list(list *l)
{
    int i;
    node* n;

    for (i = 0; i < count(l); i++) {
        n = l->nodes[i];
        free(n->data);
        free(n);
    }

    free(l->nodes);
    free(l);
}