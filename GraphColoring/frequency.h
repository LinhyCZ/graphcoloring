/*

       Transmission frequency collision solver
       Version 1.0

       Header frequency.h
       This header contains a structure that contains ID of the
       frequency entry and the frequency value

       Dialect : ANSI C
       Compiler: any ANSI C-compatible

       Copyright (c) Tomas Linhart, 2019

*/

#ifndef FREQUENCY_H
#define FREQUENCY_H

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/

/* Constants */
#define FREQUENCY_NOT_PROCCESSED    -2
#define FREQUENCY_PROCESSING        -1

/* ____________________________________________________________________________

    Structures and Datatypes
   ____________________________________________________________________________
*/

typedef struct
{
    int id;
    int frequency;
} frequency;

#endif
