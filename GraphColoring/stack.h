/*

       Transmission frequency collision solver
       Version 1.0

       Header stack.h
       for detailed description see stack.c

       Dialect : ANSI C
       Compiler: any ANSI C-compatible

       Copyright (c) Tomas Linhart, 2019

*/

#ifndef STACK_H
#define STACK_H

#include "node.h"

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/
/* Constants */
#define POINTER_SIZE    8

/* ____________________________________________________________________________

    Structures and Datatypes
   ____________________________________________________________________________
*/

typedef struct
{
    node* top;
} stack;


/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/

stack *create_stack();
void push(stack *stack, void *prvek);
void* pop(stack *stack);
int isEmpty(stack *stack);
void free_stack(stack *s);

#endif
