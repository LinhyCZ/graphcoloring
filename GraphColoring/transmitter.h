/*

       Transmission frequency collision solver
       Version 1.0

       Header transmitter.h
       Header creates struct representing transmitter that stores id of the
       transmitter, it's coordinates, 'list' of neighbors and it's frequency

       Dialect : ANSI C
       Compiler: any ANSI C-compatible

       Copyright (c) Tomas Linhart, 2019

*/

#ifndef TRANSMITTER_H
#define TRANSMITTER_H

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/
/* Constants */
#define NEIGHBORS_ARRAY_SIZE    999


#include "list.h"

/* ____________________________________________________________________________

    Structures and Datatypes
   ____________________________________________________________________________
*/

typedef struct
{
    int id;
    double x;
    double y;
    int neighbors[NEIGHBORS_ARRAY_SIZE];
    int neighbors_count;
    int frequency;
} transmitter;

#endif
