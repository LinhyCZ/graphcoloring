/*

        Transmission frequency collision solver
        Version 1.0

        Module main.c
        Main.c is an entry transmitter of the application. It reads application arguments
        and is responsible for computing all the values. It is also responsble
        for printing output back to the console.

        Dialect : ANSI C
        Compiler: any ANSI C-compatible

        Copyright (c) Tomas Linhart, 2019

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "stack.h"
#include "transmitter.h"
#include "list.h"
#include "frequency.h"
#include "parser.h"
#include "defs.h"


/* Global variables */
int transmission_radius;
list *transmitter_list;
list *frequency_list;

/* ____________________________________________________________________________

    void help()

    Prints out help message
   ____________________________________________________________________________
*/
void help()
{
    printf("\nUsage:\n   freg <filename>\n\n");
    printf("Parameters:\n   <filename> - Path to the file to be processed\n\n");
    printf("Example:\n   freq vysilace-25.txt\n\n");
}
/* ____________________________________________________________________________

    double calculate_distance(transmitter* p1, transmitter* p2)

    Returns a distance between two 'transmitters' passed as an argument
   ____________________________________________________________________________
*/
double calculate_distance(transmitter* t1, transmitter* t2)
{
    return sqrt(pow(t1->x - t2->x, 2) + pow(t1->y - t2->y, 2));
}

/* ____________________________________________________________________________

    void free_all()

    This function frees all memmory when program stops
   ____________________________________________________________________________
*/
void free_all()
{
    free_list(transmitter_list);
    free_list(frequency_list);
}

/* ____________________________________________________________________________

    void make_neighbors()

    This function loops through whole list of transmitters and calculates
    a distance to every other transmitter saved in the same list. If the
    distance is smaller than transmission radius it adds current transmitter to the
    found transmitter's neighbor list and vice versa. This makes it non-oriented graph.
   ____________________________________________________________________________
*/
void make_neighbors()
{
    int count_of_transmitters, i, j;
    double distance;

    transmitter *transmitter1, *transmitter2;
    count_of_transmitters = count(transmitter_list);

    for (i = 0; i < count_of_transmitters; i++) { /* Loop through whole list */
        transmitter1 = get(transmitter_list, i);
        for (j = i + 1; j < count(transmitter_list); j++) { /* Loop from current transmitter forward, don't go through the same combination of transmitters twice */
            transmitter2 = get(transmitter_list, j);

            distance = calculate_distance(transmitter1, transmitter2);

            if (distance <= transmission_radius * DIAMETER_MULTIPLIER) {
                transmitter1->neighbors[transmitter1->neighbors_count] = transmitter2->id;
                transmitter1->neighbors_count++;
                transmitter2->neighbors[transmitter2->neighbors_count] = transmitter1->id;
                transmitter2->neighbors_count++;
            }
        }
    }
}

/* ____________________________________________________________________________

    void colorize_graph()

    This function represents an algorithm that colorizes the graph (assigns frequency
    IDs to the transmitter). It loops through all the transmitters. First transmitter is added to the
    stack and loop starts until the stack is empty. For every transmitter in the stack all
    neighbors are checked and lowest frequency ID is assigned to the transmitter. If the
    stack is empty, transmitter that hasn't been processed yet is added to the stack and
    the loop continues. If solution isn't found, the program exits with error
    message and code 3. Otherwise stack memory is cleared and function ends.
   ____________________________________________________________________________
*/
void colorize_graph()
{
    int i, j, k;
    int frequencies[MAXIMUM_FREQUENCIES]; /* Create new array of size of maximum count of frequencies */

    int frequency_count = count(frequency_list); /* Number of frequency entries */

    stack* stack = create_stack(); /* Create stack */

    for (i = 0; i < count(transmitter_list); i++) { /* Loop through all transmitter */
        transmitter* current_transmitter = get(transmitter_list, i);
        if (current_transmitter->frequency == FREQUENCY_NOT_PROCCESSED) { /* If not proccessed yet */
            push(stack, current_transmitter);       /* add to the stack */
        }

        while (!isEmpty(stack)) {
            transmitter* stack_transmitter = (transmitter*) pop(stack); /* Get transmitter from stack */

            memset(frequencies, EMPTY, sizeof(frequencies)); /* Clear 'frequencies' array */

            for (j = 0; j < stack_transmitter->neighbors_count; j++) { /* Loop through neighbors */
                transmitter* next_transmitter = get(transmitter_list, stack_transmitter->neighbors[j]);
                if (next_transmitter->frequency == FREQUENCY_NOT_PROCCESSED) { /* If transmitter not proccessed */
                    next_transmitter->frequency = FREQUENCY_PROCESSING;        /* Set to proccessing */
                    push(stack, next_transmitter);                             /* and add to the stack */
                } else if (next_transmitter->frequency != FREQUENCY_PROCESSING) {    /* If not processing, that means has assigned frequency ID */
                    frequencies[next_transmitter->frequency] = 1;                    /* Set value in 'frequencies' at index equal to the frequency ID to 1 */
                }
            }

            for (k = 0; k < frequency_count; k++) { /* Loop through all frequencies */
                if (frequencies[k] == 0) {          /* If frequency hasn't been assigned yet */
                    stack_transmitter->frequency = k;      /* assign it to current transmitter */
                    break;
                }
            }

            if (stack_transmitter->frequency == FREQUENCY_PROCESSING) {    /* If frequency couldn't been assigned after looping through all frequencies */
                free_all();
                free_stack(stack);
                printf("ERR#3: Non-existent solution!");       /* Solution wasn't found */
                exit(ERRC_NON_EXISTENT_SOLUTION);
            }
        }
    }
    free(stack); /* Free the memory */
}

/* ____________________________________________________________________________

    void print_output_to_console()

    This function loops through the 'list' of transmitters and prints every
    transmitter entry to the console in specified format
   ____________________________________________________________________________
*/
void print_output_to_console()
{
    int i;
    for (i = 0; i < count(transmitter_list); i++) {
        transmitter* p = get(transmitter_list, i);
        frequency* f = get(frequency_list, p->frequency); /* Get frequency value by frequency ID */
        printf("%i %i\n", p->id, f->frequency);
    }
}

/* ____________________________________________________________________________

    int main(int argc, char const *argv[])

    This function composes all components of the program together. It validates
    if correct number of arguments has been passed. Then it creates two new lists,
    first for transmitters and second for frequencies. Then it parses the file,
    makes neighbors between all transmitters an colorizes the graph. At the end it prints
    out the output to the console and frees memory.
   ____________________________________________________________________________
*/

int main(int argc, char const *argv[])
{
    if (argc != ARGUMENT_COUNT) {
        printf("ERR#1: Missing argument!");
        help();
        return ERRC_MISSING_ARG;
    }

    transmitter_list = create_list();
    frequency_list = create_list();


    parse_file(argv[1], transmitter_list, frequency_list, &transmission_radius);
    make_neighbors();
    colorize_graph();
    print_output_to_console();

    free_all();
    return ERRC_SUCCESS;
}


/* TODO - MEMORY MANAGEMENT */
