/*

       Transmission frequency collision solver
       Version 1.0

       Header node.h
       Header creates a struct 'node' which is used in 'list' and 'stack'
       and contains data and pointer to the next 'node'

       Dialect : ANSI C
       Compiler: any ANSI C-compatible

       Copyright (c) Tomas Linhart, 2019

*/
#ifndef NODE_H
#define NODE_H


/* ____________________________________________________________________________

    Structures and Datatypes
   ____________________________________________________________________________
*/

typedef struct NODE
{
    void* data;
    struct NODE* next;
} node;

#endif
