/*

       Transmission frequency collision solver
       Version 1.0

       Header list.h
       for detailed description see list.c

       Dialect : ANSI C
       Compiler: any ANSI C-compatible

       Copyright (c) Tomas Linhart, 2019

*/

#ifndef LIST_H
#define LIST_H

#include "node.h"

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/

/* Constants */
#define DEFAULT_LIST_CAPACITY   50
#define RESIZE_MULTIPLIER       2
#define DEFAULT_LIST_COUNT      0
#define LIST_COUNT_EMPTY        0


/* ____________________________________________________________________________

    Structures and Datatypes
   ____________________________________________________________________________
*/

typedef struct {
    int count;
    int capacity;
    node** nodes;
} list;

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/

list *create_list();
void add(list *list, void *prvek);
void* get(list *list, int index);
int count(list *list);
void free_list(list *l);

#endif
