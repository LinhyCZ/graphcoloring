/*

        Transmission frequency collision solver
        Version 1.0

        Module parser.c
        This module loads a file from given filepath and parses
        every line of the file. It stores parsed values into
        'lists' passed as argument to 'parse_file' function.

        Dialect : ANSI C
        Compiler: any ANSI C-compatible

        Copyright (c) Tomas Linhart, 2019

 */

#include "parser.h"

#include "frequency.h"
#include "list.h"
#include "transmitter.h"
#include "defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* ____________________________________________________________________________

    static void parse_frequency(char buffer[], list* frequency_list)

    Parses buffer containing ID and value of frequency. It creates new structure
    'frequency' and puts parsed values into it. This structure is then added
    to 'frequency_list'.
   ____________________________________________________________________________
*/
static void parse_frequency(char buffer[], list* frequency_list)
{
    char* token;
    frequency* freq;
    int id;

    token = strtok(buffer, " ");
    id = atoi(token);

    token = strtok(NULL, " ");

    freq = malloc(sizeof(frequency));
    if (freq == NULL) {
        printf("ERR#2: Out of memory!");
        exit(ERRC_OUT_OF_MEMORY);
    }

    freq->id = id;
    freq->frequency = atoi(token);

    add(frequency_list, freq);
}

/* ____________________________________________________________________________

    static void parse_radius(char buffer[], int* transmission_radius)

    Parses buffer containing transmission radius of all transmitters. This
    value is then stored in 'transmission_radius' which pointer is passed
    in argument.
   ____________________________________________________________________________
*/
static void parse_radius(char buffer[], int* transmission_radius)
{
    *transmission_radius = atoi(buffer);
}

/* ____________________________________________________________________________

    static void parse_transmitter(char buffer[], list* transmitter_list)

    Parses buffer containing ID and position of a transmitter. Parsed arguments
    are ID of the transmitter and x and y coordinates. These values are then
    store in newly allocated structure 'transmitter' with frequency default value set
    to -2. This means, that the transmitter wasn't ye processed by the algorithm.
    New 'list' is also created which will serve for purpose of storing transmitters
    of neighbors when processed by algorithm. This structured is then
    added to 'transmitter_list'.
   ____________________________________________________________________________
*/
static void parse_transmitter(char *buffer, list* transmitter_list) {
    char *token, *trash;
    transmitter *new_transmitter;
    int id;
    double x, y;

    token = strtok(buffer, " ");
    id = atoi(token);
    token = strtok(NULL, " ");
    x = strtod(token, &trash);
    token = strtok(NULL, " ");
    y = strtod(token, &trash);

    new_transmitter = malloc(sizeof(transmitter));
    if (new_transmitter == NULL) {
        printf("ERR#2: Out of memory!");
        exit(2);
    }

    new_transmitter->frequency = FREQUENCY_NOT_PROCCESSED;
    new_transmitter->id = id;
    new_transmitter->x = x;
    new_transmitter->y = y;
    new_transmitter->neighbors_count = 0;

    add(transmitter_list, new_transmitter);
}


/* ____________________________________________________________________________

    static void parse_transmitter(char buffer[], list* transmitter_list)

    Parses buffer containing ID and position of a transmitter. Parsed arguments
    are ID of the transmitter and x and y coordinates. These values are then
    store in newly allocated structure 'transmitter' with frequency default value set
    to -2. This means, that the transmitter wasn't ye processed by the algorithm.
    New 'list' is also created which will serve for purpose of storing transmitters
    of neighbors when processed by algorithm. This structured is then
    added to 'transmitter_list'.
   ____________________________________________________________________________
*/
static void parse_line(char buffer[], list* transmitter_list, list* frequency_list, int* transmission_radius, int* read_mode)
{
    if (!strncmp(buffer, "Available frequencies:", AV_FREQ_TEXT_LEN)) {
        *read_mode = READ_MODE_FREQUENCIES;
    } else if (!strncmp(buffer, "Transmission radius:", TR_RAD_TEXT_LEN)) {
        *read_mode = READ_MODE_RADIUS;
    } else if (!strncmp(buffer, "Transmitters:", TRANSMITTER_TEXT_LEN)) {
        *read_mode = READ_MODE_TRANSMITTERS;
    } else {
        switch (*read_mode) {
            case READ_MODE_FREQUENCIES:
                parse_frequency(buffer, frequency_list);
                break;
            case READ_MODE_RADIUS:
                parse_radius(buffer, transmission_radius);
                break;
            case READ_MODE_TRANSMITTERS:
                parse_transmitter(buffer, transmitter_list);
                break;
        }
    }
}


/* ____________________________________________________________________________

    void parse_file(const char *filepath, list* transmitter_list, list* frequency_list, int* transmission_radius)

    This function creates 'FILE' pointer and reads file specified in argument.
    If file is not found, it prints out a message and program exits with code 4.
    Then it loops through the file line by line using function 'fgets' and stores
    data into buffer allocated at the top of the function. This buffer is
    then passed to function 'parse_line'. In the end it closes file connection.
   ____________________________________________________________________________
*/
void parse_file(const char *filepath, list* transmitter_list, list* frequency_list, int* transmission_radius)
{
    FILE* fp;
    char buffer[DEFAULT_BUFFER_SIZE];
    int read_mode = READ_MODE_FREQUENCIES;

    fp = fopen(filepath, "r");

    if (fp == NULL) {
        printf("ERR#4: File not found!");
        exit(ERRC_FILE_NOT_FOUND);
    }
    while (fgets(buffer, DEFAULT_BUFFER_SIZE, (FILE*) fp)) {
        parse_line(buffer, transmitter_list, frequency_list, transmission_radius, &read_mode);
    }

    fclose(fp);
}