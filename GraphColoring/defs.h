/*

       Transmission frequency collision solver
       Version 1.0

       Header defs.h
       This header contains definitions of constants.

       Dialect : ANSI C
       Compiler: any ANSI C-compatible

       Copyright (c) Tomas Linhart, 2019

*/

#ifndef DEFS_H
#define DEFS_H

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/
/* Constants */

#define DIAMETER_MULTIPLIER         2
#define EMPTY                       0
#define ARGUMENT_COUNT              2
#define ERRC_SUCCESS                0
#define ERRC_MISSING_ARG            1
#define ERRC_OUT_OF_MEMORY          2
#define ERRC_NON_EXISTENT_SOLUTION  3
#define ERRC_FILE_NOT_FOUND         4
#define MAXIMUM_FREQUENCIES         999

#endif
