/*

        Transmission frequency collision solver
        Version 1.0

        Module stack.c
        This module represents a stack implemented by a linked list.
        Pointer to the top node is stored in variable. It is possible
        to add and remove elements from the stack using functions
        push and pop.

        Dialect : ANSI C
        Compiler: any ANSI C-compatible

        Copyright (c) Tomas Linhart, 2019

 */
#include <stdlib.h>
#include <stdio.h>

#include "stack.h"

/* ____________________________________________________________________________

    stack *create_stack()

    This function allocates and initializes space in memory to a new 'stack'.
    If program runs out of memory, it exits with an error code and message.
   ____________________________________________________________________________
*/
stack *create_stack()
{
    stack* stack = calloc(1, sizeof(stack));
    if (stack == NULL) {
        printf("ERR#2: Out of memory!");
        exit(2);
    }

    return stack;
}

/* ____________________________________________________________________________

    void push(stack *stack, void *prvek)

    This function creates new 'node'. Pointer to data is then inserted into the node.
    Node which was on top is added to next parameter of the new node and the new node
    becomes new top node.
   ____________________________________________________________________________
*/
void push(stack *stack, void *prvek)
{
    node* node;

    node = malloc(sizeof(node) + POINTER_SIZE);
    if (node == NULL) {
        printf("ERR#2: Out of memory!");
        exit(2);
    }

    node->data = prvek;
    node->next = stack->top;
    stack->top = node;
}

/* ____________________________________________________________________________

    void* pop(stack *stack)

    This function returns pointer to data of the top node. Data pointer is
    stored in varaible and the memory of the old top node is freed. Node which
    was tops next node becomes new top node.
   ____________________________________________________________________________
*/
void* pop(stack *stack)
{
    node* node;
    void* data;
    if (stack->top == NULL) {
        return NULL;
    }
    node = stack->top;
    data = node->data;
    stack->top = node->next;

    free(node);

    return data;
}


/* ____________________________________________________________________________

    int isEmpty(stack *s)

    This function checks if top node is NULL. If it is, it returns 1, otherwise returns 0
   ____________________________________________________________________________
*/
int isEmpty(stack *s)
{
    if (s->top == NULL) {
        return 1;
    }
    return 0;
}

/* ____________________________________________________________________________

    void free_stack(stack *s)

    This function frees data of stack
   ____________________________________________________________________________
*/
void free_stack(stack *s)
{
    node* node;

    while (!isEmpty(s)) {
        node = s->top;
        s->top = node->next;

        free(node);
    }
    free(s);
}
