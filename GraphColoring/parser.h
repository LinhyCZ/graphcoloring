/*

       Transmission frequency collision solver
       Version 1.0

       Header parser.h
       for detailed description see parser.c

       Dialect : ANSI C
       Compiler: any ANSI C-compatible

       Copyright (c) Tomas Linhart, 2019

*/
#ifndef PARSER_H
#define PARSER_H

#include "list.h"


/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/
/* Constants */
#define DEFAULT_BUFFER_SIZE     50
#define READ_MODE_FREQUENCIES   0
#define READ_MODE_RADIUS        1
#define READ_MODE_TRANSMITTERS  2
#define AV_FREQ_TEXT_LEN        22
#define TR_RAD_TEXT_LEN         20
#define TRANSMITTER_TEXT_LEN    13

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/

void parse_file(const char *filepath, list* transmitter_list, list* frequency_list, int* transmission_radius);

#endif
