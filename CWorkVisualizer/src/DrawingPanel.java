import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class DrawingPanel extends JPanel {
    private HashMap<Integer, Integer> data;
    private HashMap<Integer, Color> frequencies;
    private HashMap<Integer, Double[]> transmitters;
    private ArrayList<Integer> frequencyList;

    private static Color[] colorArray = new Color[] {new Color(0, 0, 255), new Color(0, 153, 0), new Color(255,0,0), new Color(255, 204,0)};
    private static Color[] lightColorArray = new Color[] {new Color(0, 0, 255,60), new Color(0, 153, 0,60), new Color(255,0,0,60), new Color(255, 204,0,60)};

    int transmissionRadius;

    public DrawingPanel(int transmissionRadius, HashMap<Integer, Integer> data, HashMap<Integer, Color> frequencies, HashMap<Integer, Double[]> transmitters, ArrayList<Integer> frequencyList) {
        this.transmissionRadius = transmissionRadius;
        this.data = data;
        this.frequencies = frequencies;
        this.transmitters = transmitters;
        this.frequencyList = frequencyList;
    }


    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        Iterator<Integer> dataIterator = data.keySet().iterator();

        while (dataIterator.hasNext()) {
            int pointID = dataIterator.next();
            int frequency = data.get(pointID);
            //Color color = frequencies.get(frequency);

            float value = (float) frequencyList.indexOf(frequency) / frequencyList.size(); //this is your value between 0 and 1
            float minHue = 0; //corresponds to green
            float maxHue = 255; //corresponds to red
            float hue = value*maxHue + (1-value)*minHue;
            Color color = new Color(Color.HSBtoRGB(hue, 1, 0.5f));


            Double[] coords = transmitters.get(pointID);

            final double systemDiameter = Math.max(visualizer.maxRX - visualizer.minRX, visualizer.maxRY - visualizer.minRY);

            final double scale = (Math.min(this.getWidth(), this.getHeight()) / systemDiameter) * 0.95;

            double h = this.getHeight();

            double xSmall = (coords[0] * scale) + 100;
            double ySmall = h - (coords[1] * scale);

            double radius = transmissionRadius * scale;

            double x = (coords[0] * scale) - (radius / 2) + 100;
            double y = (h - (coords[1] * scale)) - (radius / 2);


            //Color color = lightColorArray[frequencyList.indexOf(frequency)];
            g2.setColor(new Color(color.getRed() / (float) 255, color.getGreen() / (float) 255, color.getBlue() / (float) 255, (float) 0.1));

            //color = colorArray[frequencyList.indexOf(frequency)];
            //g2.draw(new Ellipse2D.Double(coords[0] - transmissionRadius / 2, coords[1] - transmissionRadius / 2, transmissionRadius, transmissionRadius));
            g2.fill(new Ellipse2D.Double(x, y, radius, radius));

            g2.setColor(color);


            g2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
            g2.drawString(pointID + "", (int) xSmall + 5, (int) ySmall + 5);
            g2.fill(new Ellipse2D.Double(xSmall - 5, ySmall - 5, 10, 10));
        }
    }
}

