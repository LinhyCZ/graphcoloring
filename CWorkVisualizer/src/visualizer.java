import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class visualizer {
    private static HashMap<Integer, Integer> data = new HashMap<>();
    private static HashMap<Integer, Double[]> transmitters = new HashMap<>();
    private static HashMap<Integer, Color> frequencies = new HashMap<>();
    private static ArrayList<Integer> frequencyList = new ArrayList<>();
    private static int transmissionRadius;

    private static Color[] colorArray = new Color[] {Color.BLUE, new Color(0, 153, 0), Color.RED, new Color(255, 204,0)};

    public static double minRX = Double.MAX_VALUE;
    public static double minRY = Double.MAX_VALUE;
    public static double maxRX = Double.MIN_VALUE;
    public static double maxRY = Double.MIN_VALUE;


    public static void main(String[] args) {
        parseFile();

        JFrame frame = new JFrame();

        DrawingPanel panel = new DrawingPanel(transmissionRadius, data, frequencies, transmitters, frequencyList);
        panel.setPreferredSize(new Dimension(700, 500));
        frame.add(panel);

        frame.setTitle("Mé první okno :)");
        //frame.setSize(640, 480);
        frame.pack(); //Automaticky nastaví šířku a výšku podle obsahu okna

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null); //Center na stred obrazovky

        frame.setVisible(true);

    }

    public static void parseFile() {
        try {
            File file = new File("vysledek-test.txt");
            Scanner sc = new Scanner(file);

            while (sc.hasNext()) {
                String line = sc.nextLine();
                //System.out.println("Line:" + line);

                data.put(Integer.parseInt(line.split(" ")[0]), Integer.parseInt(line.split(" ")[1]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            int readMode = 0;
            File file = new File("vysledek-vstup.txt");
            Scanner sc = new Scanner(file);

            int colorID = 0;

            while (sc.hasNext()) {
                String line = sc.nextLine();
                //System.out.println("Line:" + line);
                if (line.equals("Available frequencies:")) {
                    readMode = 0;
                } else if (line.equals("Transmission radius:")) {
                    readMode = 1;
                } else if (line.equals("Transmitters:")) {
                    readMode = 2;
                } else {
                    switch (readMode) {
                        case 0:
                            frequencies.put(Integer.parseInt(line.split(" ")[1]), new Color(0,0,0));
                            frequencyList.add(Integer.parseInt(line.split(" ")[1]));
                            colorID++;
                            break;
                        case 1:
                            transmissionRadius = Integer.parseInt(line) * 2;
                            break;
                        case 2:
                            double x = Double.parseDouble(line.split(" ")[1]);
                            if (x > maxRX) {
                                maxRX = x;
                            }

                            if (x < minRX) {
                                minRX = x;
                            }

                            double y = Double.parseDouble(line.split(" ")[2]);
                            if (y > maxRY) {
                                maxRY = y;
                            }

                            if (y < minRY) {
                                minRY = y;
                            }

                            int id = Integer.parseInt(line.split(" ")[0]);
                            transmitters.put(id, new Double[] {x, y});
                            break;
                        default:
                            System.out.println("WTF");
                    }
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("Chyba: Nenalezen soubor!");
            e.printStackTrace();
        }
    }
}
