import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.*;

class Point {
    final int id;
    final double x;
    final double y;
    final List<Point> neighbors = new LinkedList<>();
    int frequency = -2;

    Point(double x, double y, int id) {
        this.x = x;
        this.y = y;
        this.id = id;
    }
}

public class Main {
    static List<Integer> availableFrequencies = new LinkedList<>();
    static int transmissionRadius = 0;
    static List<Point> transmitters = new LinkedList<>();
    static String filename;


    public static void main(String[] args) throws FileNotFoundException {
        if (args.length != 1) {
            System.out.println("Špatné argumenty");
            return;
        }

        filename = args[0];

        long millis = System.currentTimeMillis();

        readFile();
        //testFileRead();
        makeNeighbors();
        colorizeGraph();
        //outputGraphToConsole();
        outputResultToFile();

        System.out.println("Výpočet zabral " + (System.currentTimeMillis() - millis) + " ms");

    }

    private static void readFile() {
        System.out.println("/==============================\\");
        System.out.println("|        Čtení souboru         |");
        System.out.println("\\==============================/");
        System.out.println();
        try {
            int readMode = 0;
            File file = new File(filename);
            Scanner sc = new Scanner(file);

            while (sc.hasNext()) {
                String line = sc.nextLine();
                //System.out.println("Line:" + line);
                if (line.equals("Available frequencies:")) {
                    readMode = 0;
                } else if (line.equals("Transmission radius:")) {
                    readMode = 1;
                } else if (line.equals("Transmitters:")) {
                    readMode = 2;
                } else {
                    switch (readMode) {
                        case 0:
                            availableFrequencies.add(Integer.parseInt(line.split(" ")[1]));
                            break;
                        case 1:
                            transmissionRadius = Integer.parseInt(line);
                            break;
                        case 2:
                            double x = Double.parseDouble(line.split(" ")[1]);
                            double y = Double.parseDouble(line.split(" ")[2]);
                            int id = Integer.parseInt(line.split(" ")[0]);
                            transmitters.add(new Point(x, y, id));
                            break;
                        default:
                            System.out.println("WTF");
                    }
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("Chyba: Nenalezen soubor!");
            e.printStackTrace();
        }
    }

    private static void testFileRead() {
        System.out.println();
        System.out.println("/==============================\\");
        System.out.println("|   Testování načtení vstupu   |");
        System.out.println("\\==============================/");
        System.out.println();

        System.out.println("Available frequencies:");
        System.out.println(Arrays.toString(availableFrequencies.toArray()));
        System.out.println("Transimission radius:");
        System.out.println(transmissionRadius);
        System.out.println("Transmitters:");
        Iterator<Point> it = transmitters.iterator();
        while (it.hasNext()) {
            Point next = it.next();
            System.out.println(next.x + ", " + next.y);
        }
    }

    private static void makeNeighbors() {
        for (int i = 0; i < transmitters.size(); i++) {
            Point point1 = transmitters.get(i);
            for (int j = i + 1; j < transmitters.size(); j++) {
                Point point2 = transmitters.get(j);
                System.out.print("Kontroluji body: " + i + " (" + point1.x +", " + point1.y + ") a " + j + " (" + point2.x +", " + point2.y + ")");
                double distance = calculateDistance(point1, point2);
                System.out.print(", vzdálenost: " + distance);
                if (distance <= transmissionRadius * 2) {
                    point1.neighbors.add(point2);
                    point2.neighbors.add(point1);
                    System.out.println(", Přidávám!");
                } else {
                    System.out.println();
                }
            }
        }
    }

    private static double calculateDistance(Point p1, Point p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    private static void colorizeGraph() {
        //Queue<Point> stack = new LinkedList<>();
        Stack<Point> stack = new Stack<>();
        for (int i = 0; i < transmitters.size(); i++) {
            Point currentPoint = transmitters.get(i);
            if (currentPoint.frequency == -2) {
                stack.add(currentPoint);
                //System.out.println("Přidávám bod " + currentPoint.id + " do stacku (1)");
            }

            while (!stack.isEmpty()) {
                //Point stackPoint = stack.remove();
                Point stackPoint = stack.pop();
                /*System.out.println("Vybírám bod " + stackPoint.id + " ze stacku");

                Iterator<Point> itn = stackPoint.neighbors.iterator();

                while (itn.hasNext()) {
                    System.out.println("Soused bodu " + stackPoint.id + " je " + itn.next().id);
                }*/

                boolean[] frequencies = new boolean[availableFrequencies.size()];

                for (int j = 0; j < stackPoint.neighbors.size() ; j++) {
                    Point nextPoint = stackPoint.neighbors.get(j);
                    //System.out.println("Získávám bod " + nextPoint.id);
                    if (nextPoint.frequency == -2) {
                        nextPoint.frequency = -1;
                        stack.add(nextPoint);
                        //System.out.println("Přidávám bod " + nextPoint.id + " do stacku (2)");
                    } else if (nextPoint.frequency != -1) {
                        frequencies[nextPoint.frequency] = true;
                    }
                }

                for (int k = 0; k < frequencies.length; k++) {
                    if (!frequencies[k]) {
                        stackPoint.frequency = k;
                        //System.out.println("Frekvence bodu " + stackPoint.id + " = " + stackPoint.frequency);
                        break;
                    }
                }

                if (stackPoint.frequency == -1) {
                    System.out.println("Nemohl jsem najít frekvenci!");
                }
            }
        }
    }

    private static void outputGraphToConsole() {
        System.out.println();
        System.out.println("/=======================================\\");
        System.out.println("|   Výpis obarveného grafu do konzole   |");
        System.out.println("\\=======================================/");
        System.out.println();
        Iterator<Point> it = transmitters.iterator();
        int i = 0;
        while (it.hasNext()) {
            Point p = it.next();

            System.out.println(i + " " + availableFrequencies.get(p.frequency));
            i++;
        }
    }

    private static void outputResultToFile() throws FileNotFoundException {
        PrintStream out = new PrintStream(new FileOutputStream(filename.split(".txt")[0] + "-vysledek.txt"));

        for (int i = 0; i < transmitters.size(); i++) {
            Point p = transmitters.get(i);

            out.println(p.id + " " + availableFrequencies.get(p.frequency));
        }
    }
}
